import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { HomePage } from './home.page';
import { HomePageRoutingModule } from './home-routing.module';
import { MessageComponentModule } from '../message/message.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {ComponentsModule } from '../components/components.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MessageComponentModule,
        HomePageRoutingModule,
        FontAwesomeModule,
        ComponentsModule
    ],
    declarations: [HomePage]
})
export class HomePageModule {}
