import { Component } from '@angular/core';
import { DataService, Slides } from '../services/data.service';
import { faBars } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  menuIcon: any = faBars;
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  expanded = false;
  auxExp = false;

  constructor(private data: DataService) {}

  refresh(ev) {
    setTimeout(() => {
      ev.detail.complete();
    }, 3000);
  }

  getSlides(): Slides[] {
    return this.data.getSlides();
  }

  expClick() {
    if(this.auxExp === false) {
      this.expanded = true;
      this.auxExp = true;
    } else {
      this.expanded = false;
      this.auxExp = false;
    }
  }

}
