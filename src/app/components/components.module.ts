import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ComponentsExpandableComponent } from './components-expandable/components-expandable.component';
@NgModule({
  declarations: [
    ComponentsExpandableComponent],
  imports: [
    IonicModule
  ],
  exports: [
    ComponentsExpandableComponent]
})
export class ComponentsModule {}
