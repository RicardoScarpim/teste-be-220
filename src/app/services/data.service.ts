import { Injectable } from '@angular/core';

export interface Slides {
  name: string;
  new: boolean;
  create: boolean;
  slides: [{
    title: any;
    training: any;
  }, {
    title: any;
    training: any;
  }, {
    title: any;
    training: any;
  }];
}

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public slides: Slides[] = [
    {
      name: 'PERSONAL ONLINE',
      new: false,
      create: true,
      slides: [{
        title: 'YOGA TESTE',
        training: false
      }, {
        title: 'ACADEMIA TESTE',
        training: false
      }, {
        title: 'CROSSFIT TESTE',
        training: false
      }]
    },{
      name: 'PROGRAMAS',
      new: true,
      create: false,
      slides: [{
        title: 'LEVANTAMENTO DE PESO',
        training: true
      }, {
        title: 'METROS RASOS',
        training: false
      }, {
        title: 'BURPEE',
        training: false
      }]
    },{
      name: 'CONTEÚDOS',
      new: false,
      create: false,
      slides: [{
        title: 'TREINO DE PERNA',
        training: false
      }, {
        title: 'TREINO DE PEITO',
        training: false
      }, {
        title: 'TREINO DE BICEPS',
        training: false
      }]
    }
  ];

  constructor() { }

  public getSlides(): Slides[] {
    return this.slides;
  }

  public getSlidesById(id: number): Slides {
    return this.slides[id];
  }
}
